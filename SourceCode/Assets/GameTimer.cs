﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameTimer : MonoBehaviour {


    public SandTimer _sandTimer;
    public PlayersScoreBoard _scoreBoard;
    public Text _winnerDisplay;
    public GameObject _rootOfVictoryToActivated;

    public float _timeTofinishGame = 300f;
    public float _timeLeft = 300f;

    void Start() {
        _timeLeft = _timeTofinishGame;
    }

    void Update () {

        if(_timeLeft>0f)
        {
            _timeLeft -= Time.deltaTime;
            _sandTimer.SetTimerTo(1f-(_timeLeft / _timeTofinishGame));

            if (_timeLeft <= 0f) {
                List<string> winnerList =_scoreBoard.GetPlayersIdNameSortByScore(3);
                if (winnerList.Count == 0)
                    _winnerDisplay.text = "No winner";
                if (winnerList.Count >0)
                    _winnerDisplay.text = "Winner is "+winnerList[0]+" !" ;
                _winnerDisplay.gameObject.SetActive(true);
                _rootOfVictoryToActivated.SetActive(true);
                Invoke("RestartGame", 5);
            }
        }
	}
    void RestartGame() {

        _scoreBoard.ResetDate();

        _winnerDisplay.gameObject.SetActive(false);
        _rootOfVictoryToActivated.SetActive(false);
        _timeLeft = _timeTofinishGame; 
    }
}
