﻿using UnityEngine;
using System.Collections;

public class EnterToChat : MonoBehaviour {

    public TwitchIRC _twitchIRC;
    public string _messageToSend;

	void Update () {

        if (Input.anyKey) {
            if (Input.GetKeyDown(KeyCode.Return)) {
                SendInputTyped();
            }

            _messageToSend += Input.inputString ;

        }
    }
	
    public void SendInputTyped()
    {
        if (string.IsNullOrEmpty(_messageToSend)) return;
        print(_messageToSend);
        _twitchIRC.SendMsg(_messageToSend);
        _messageToSend = "";

    }
}