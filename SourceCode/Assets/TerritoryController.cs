using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TerritoryController : MonoBehaviour
{
    static Dictionary<string, TerritoryController> _registeredTerritory = new Dictionary<string, TerritoryController>();

    public TerritoryZone _zoneInformation;
    public UI_TerritoryZone _zoneUiInterface;
    public ArmyInZone _armyData;

    [Header("Debug Button")]
    public bool _showMe;

    public string GetTerritoryID() {
        return _zoneInformation.GetTerritoryAlias();
    }
    void Awake() {
        _registeredTerritory.Add(GetTerritoryID() ,this );
    }
    void Start()
    {
        Refresh();
    }

    

    public void OnValidate() {

        if (_showMe)
        {
            _showMe = false;
            //   #if UNITY_EDITOR
            DrawDebugLineToNearTerritories(GetTerritoryID());
            //  #endif
        }
    }

    public static  TerritoryController GetTerritoryById(string id) {
        TerritoryController resulta = null;
        if (_registeredTerritory.ContainsKey(id))
            resulta = _registeredTerritory[id];
        return resulta;
    }

    private static void DrawDebugLineToNearTerritories(string territoryId) {
        DrawDebugLineToNearTerritories(territoryId, Color.green, 10f);
    }
    private static void DrawDebugLineToNearTerritories(string territoryId, Color color, float drawDuration)
    {
        TerritoryController targettedTerritory = GetTerritoryById(territoryId);
      
        Transform center = targettedTerritory._zoneUiInterface.GetRootOfTheCountry();
        List<string> nearTerritory = targettedTerritory._zoneInformation.GetNearTerritoriesAlias();

        for (int i = 0; i < nearTerritory.Count; i++)
        {
           
            string nearTerritoryId = nearTerritory[i];
            TerritoryController territory = GetTerritoryById(nearTerritoryId);
            if (territory != null)
            {
                Transform nearCenter = territory.GetRootOfTheTerritory();
                Debug.DrawLine(center.position, nearCenter.position, color, drawDuration);
                Debug.Log(nearTerritoryId +"ii "+ nearCenter.position);
            }
        }

    }

    public Transform GetRootOfTheTerritory() {
        return _zoneUiInterface.GetRootOfTheCountry(); 
    }



    public void Refresh()
    {
        _zoneUiInterface.SetTerritoryTo(_armyData.GetNumberOfUnit(), Color.white);
        _zoneUiInterface.SetDisplayTextTo(""+GetTerritoryID()+":"+_armyData.GetNumberOfUnit());
    }
}
