﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class SandTimer : MonoBehaviour
{

    [SerializeField]
    [Range(0f, 1f)]
    private float _pourcentLoaded;



    public void SetTimerTo(float pourcentLoaded)
    {

        _pourcentLoaded = pourcentLoaded;
        FillTopPartOfSandTimer(1f - _pourcentLoaded);
        FillBotPartOfSandTimer(_pourcentLoaded);
    }

    public float GetTimerState() { return _pourcentLoaded; }

    public void OnValidate()
    {
        SetTimerTo(_pourcentLoaded);
    }

    public abstract void FillTopPartOfSandTimer(float pourcent);
    public abstract void FillBotPartOfSandTimer(float pourcent);
}
