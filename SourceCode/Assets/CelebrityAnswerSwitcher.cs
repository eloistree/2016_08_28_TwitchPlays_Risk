﻿using UnityEngine;
using System.Collections;

public class CelebrityAnswerSwitcher : MonoBehaviour {

    public PlayersStateManager _players;
    public CelebrityFinderGame _celebrityControler;
    public RandomCelebritySetup _celebrities;


    public Coroutine _switchCelebrity;
    
	void Start () {

        _players._onUserSendMessage += OnMessageReceived;
        _celebrityControler._onValueChange.AddListener(CheckCelebrityFound);
        SwitchDirectly();
    }

    private void OnMessageReceived(string userName, string message)
    {
        if (message.ToLower().Contains("#next") && userName.ToLower().Contains("jamscenter"))
        {
            SwitchDirectly();
        }
    }

        void CheckCelebrityFound() {
        if (_celebrityControler.IsCelebrityFound()) {
            if (_switchCelebrity == null)
                _switchCelebrity = StartCoroutine(Switch());
            
        }

    }
    public void SwitchDirectly()
    {
        _celebrityControler.SetCelebrityTo(_celebrities.GetRandom());


    }
    public IEnumerator Switch (){
        yield return new WaitForSeconds(3);
        SwitchDirectly();
        _switchCelebrity = null;

    }
}
