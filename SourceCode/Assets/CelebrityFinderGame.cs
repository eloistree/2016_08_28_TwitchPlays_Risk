﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class CelebrityFinderGame : MonoBehaviour {


    [SerializeField]
    private CelebrityData _selectedCelebrity;
    public bool[] _celebrityNameLetterFind;

    public char _hiddenLetter = '_';

    internal void SetRandomLetterAsFounded()
    {
        if (_celebrityNameLetterFind == null || _celebrityNameLetterFind.Length <= 0)
            return;

        int maxCount = 7;
        int indexToReveal=0;

        while (maxCount > 0 && _celebrityNameLetterFind[indexToReveal] == true)
        {
            indexToReveal = UnityEngine.Random.Range(0, _celebrityNameLetterFind.Length);
            maxCount--;

        }
        SetTheLetterAsFounded(indexToReveal);


    }

    public string GetCelebrityName()
    {
        return _selectedCelebrity._name.GetValue();
    }

    public UnityEvent _onValueChange;


    public bool IsTheLetterValide(char letter, int index) {
        Debug.Log(letter + "-letter- index>" + index);
       return  _selectedCelebrity._name.IsItTheGoodLetter(letter, index);
    }

    public void SetTheLetterAsFounded (int index) {
        if(index>=0 && index<_celebrityNameLetterFind.Length )
        _celebrityNameLetterFind [index]= true;
        _onValueChange.Invoke();
    }

    public string GetHiddenName() {
       return  _selectedCelebrity._name.GetHiddenStringFormat(_celebrityNameLetterFind, '.');
    }

    internal bool IsLetterAlreadyFound(int index)
    {
        if (index < 0 || index >= _celebrityNameLetterFind.Length) return true;
        else return _celebrityNameLetterFind[index];
    }

    public void OnValidate() {
        _onValueChange.Invoke();
    }

    internal Sprite GetImageOfCelebrity()
    {
        return _selectedCelebrity._image;
    }

    public void SetCelebrityTo(CelebrityData celebrityData)
    {
        _selectedCelebrity = celebrityData;
        _celebrityNameLetterFind = new bool[_selectedCelebrity._name.Length];
        _onValueChange.Invoke();
    }

    public bool IsCelebrityFound()
    {
        for (int i = 0; i < _celebrityNameLetterFind.Length; i++)
        {
            if (!_celebrityNameLetterFind[i])
                return false;
        }
        return true;
    }
    

    public float GetPourcentageDiscovered()
    {
        int foundCount = 0;
        for (int i = 0; i < _celebrityNameLetterFind.Length; i++)
        {
            if (_celebrityNameLetterFind[i])
                foundCount++;
        }
        return ((float)foundCount) / ((float)_celebrityNameLetterFind.Length);
    }
}
