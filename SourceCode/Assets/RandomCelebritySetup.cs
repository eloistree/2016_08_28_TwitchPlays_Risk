﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class RandomCelebritySetup : MonoBehaviour {

    public CelebrityFinderGame _celebrityFinder;
    public CelebrityData[] _celebrites;

    public  CelebrityData GetRandom()
    {
        return _celebrites[UnityEngine.Random.Range(0, _celebrites.Length)];
    }
}

[System.Serializable]
public class CelebrityData {

    [SerializeField]
    [Tooltip("The name of the celebrity to find out.")]
    public HangManString _name;
    [SerializeField]
    [Tooltip("Image that is hidden behind the name")]
    public Sprite _image;
    public AudioClip _cultSentence;

}
