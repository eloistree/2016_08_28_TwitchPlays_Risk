﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ImageColorSwitcher : MonoBehaviour {

    public Image _imageAffected;

    public Color _wantedColor= Color.white;
    public Color _origineColor = Color.white;
    public float _timeToConvert = 1f;
    public float _convertionTimeState = 0f;

    public Color GetCurrentColor() { return _imageAffected.color; }
    public void  SetCurrentColor(Color color) { _imageAffected.color= color; }

    public void Update() {

        if (_convertionTimeState > _timeToConvert) return;

        _convertionTimeState += Time.deltaTime;
        _convertionTimeState = Mathf.Clamp(_convertionTimeState, 0f, _timeToConvert);

        Color colToSet = Color.Lerp(_origineColor, _wantedColor, _convertionTimeState / _timeToConvert);
        SetCurrentColor(colToSet);
        
    }


    public void CancelConvertion()
    {
        SetCurrentColor(_origineColor);
        _wantedColor = _origineColor;
        _convertionTimeState = 1f;
    }

    void OnValidate()
    {
        if (_wantedColor != GetCurrentColor()) 
        SetColorTo(_wantedColor, 1f);
    }

     public void SetColorTo(Color wantedColor, float timeToConvert)
    {
        if (_imageAffected == null)
            return;
        _origineColor = GetCurrentColor();
        _wantedColor = wantedColor;
        _timeToConvert = timeToConvert;
        _convertionTimeState = 0f;
    }
    public void SetColorTo(Color origineColor, Color wantedColor, float timeToConvert)
    {
        SetCurrentColor(origineColor);
        SetColorTo( wantedColor, timeToConvert );
    }
}
