﻿using UnityEngine;
using System.Collections;
using System;

public class BotHelper : MonoBehaviour {


    public TwitchIRC _twitchIRC;
    public PlayersStateManager _playerManager;
    public CelebrityFinderGame _celebrityFinder;

    public float lastTimeRequested;
    public void Start() {

        _playerManager._onUserSendMessage += ListenToPlayer;

    }

    private void ListenToPlayer(string userName, string message)
    {
        if (message.ToLower().Contains("#help"))
        {
            if (Time.time - lastTimeRequested > 5f)
            {
                lastTimeRequested = Time.time;

                string helpingMessage = "Here is a clue: ";
                string celebrityName = _celebrityFinder.GetCelebrityName();
                
                helpingMessage += MixTheString(celebrityName);
                _twitchIRC.SendMsg(helpingMessage);
            }
        }
        if (message.ToLower().Contains("#time"))
        {
            if (Time.time - lastTimeRequested > 5f)
            {
                lastTimeRequested = Time.time;

                
                _twitchIRC.SendMsg("Time:" + DigitalClock.GetCurrentTime(true, true, true, false).GetTimeAsString());
            }
        }

    }

    private string MixTheString( string celebrityName)
    {
        char[] nameTable = celebrityName.ToCharArray();

        for (int i = 0; i < 66; i++)
        {
            
            int indexOne =UnityEngine.Random.Range(0, nameTable.Length);
            int indexTwo = UnityEngine.Random.Range(0, nameTable.Length);
            char tmp = nameTable[indexOne];
            nameTable[indexOne] = nameTable[indexTwo];
            nameTable[indexTwo] = tmp;
        }


        return new string(nameTable);
    }
}
