﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class DigitalClock
{
    [SerializeField]
    private bool _allowHours = true;
    public bool IsAllowingHours() { return _allowHours; }
    [SerializeField]
    [Range(0, 23)]
    private int _hours;
    public int GetHours() { return _hours; }

    [SerializeField]
    private bool _allowMinutes = true;
    public bool IsAllowingMinutes() { return _allowMinutes; }

    public void SetTimerWithSeconds(int secondleft)
    {
        int hour = (int)(((float)secondleft) / 3600f);
        secondleft -= hour * 3600;
        int minute = (int)(((float)secondleft) / 60f);
        secondleft -= minute * 60;
        int second = secondleft;

        SetHoursTo(hour).SetMinutesTo(minute).SetSecondesTo(second);
        
    }

    [SerializeField]
    [Range(0, 59)]
    private int _minutes;
    public int GetMinutes() { return _minutes; }

    [SerializeField]
    private bool _allowSeconds;
    public bool IsAllowingSeconds() { return _allowSeconds; }
    [SerializeField]
    [Range(0, 59)]
    private int _seconds;
    public int GetSeconds() { return _seconds; }
    [SerializeField]

    private bool _allowMilliseconds;
    public bool IsAllowingMilliseconds() { return _allowMilliseconds; }
    [SerializeField]
    [Range(0, 999)]
    private int _milliseconds;
    public int GetMilliseconds() { return _milliseconds; }



    public DigitalClock SetHoursTo(int hours)
    {
        _hours = Mathf.Clamp(hours, 0, 24);
        return this;
    }
    public DigitalClock SetMinutesTo(int minutes)
    {
        _minutes = Mathf.Clamp(minutes, 0, 60);
        return this;
    }
    public DigitalClock SetSecondesTo(int seconds)
    {
        _seconds = Mathf.Clamp(seconds, 0, 60);
        return this;
    }
    public DigitalClock SetMillisecondsTo(int milliseconds)
    {
        _milliseconds = Mathf.Clamp(milliseconds, 0, 1000);
        return this;
    }

    public string GetTimeAsString(string seperator = ":")
    {
        string textStructured = string.Format("{0:00}{1:00}{2:00}{3:00}",
           IsAllowingHours() ? GetHours().ToString("D2") + seperator : "",
           IsAllowingMinutes() ? GetMinutes().ToString("D2") + seperator : "",
           IsAllowingSeconds() ? GetSeconds().ToString("D2") + seperator : "",
           IsAllowingMilliseconds() ? GetMilliseconds().ToString("D3") : ""
       );
        textStructured = textStructured.Trim(':');
        return textStructured;
    }


    public static DigitalClock GetCurrentTime(bool hour, bool minute, bool second, bool millisecond) {
        DigitalClock clock = new DigitalClock();
        clock._allowHours = hour;
        clock._allowMinutes = minute;
        clock._allowSeconds = second;
        clock.SetHoursTo(DateTime.Now.Hour).SetMinutesTo(DateTime.Now.Minute).SetSecondesTo(DateTime.Now.Second);
        return clock;
    }
    
}
